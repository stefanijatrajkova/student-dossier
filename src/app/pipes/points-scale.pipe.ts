import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'pointsScale'
})
export class PointsScalePipe implements PipeTransform {
    transform(value: number, scaleFactor: number): number {
        const scaledPoints = value * scaleFactor;
        return scaledPoints < 5 ? 5 : scaledPoints > 10 ? 10 : Math.round(scaledPoints);
    }
}
