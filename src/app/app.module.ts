import {LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {FormsModule} from "@angular/forms";
import {StudentDossierComponent} from './student-dossier/student-dossier.component';
import {registerLocaleData} from "@angular/common";
import localeDe from '@angular/common/locales/de';
import {PointsScalePipe} from './pipes/points-scale.pipe';

registerLocaleData(localeDe, 'de');

@NgModule({
    declarations: [
        AppComponent,
        StudentDossierComponent,
        PointsScalePipe
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        MatCardModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        FormsModule,
        NoopAnimationsModule
    ],
    providers: [{
        provide: LOCALE_ID,
        useValue: 'de_DE'
    }],
    bootstrap: [AppComponent]
})
export class AppModule {
}
