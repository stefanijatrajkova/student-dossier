import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {StudentDossier} from "../models/StudentDossier";

export let MAX_POINTS = 100;

@Component({
    selector: 'app-student-dossier',
    templateUrl: './student-dossier.component.html',
    styleUrls: ['./student-dossier.component.css']
})
export class StudentDossierComponent implements OnInit {
    @Input() studentDossier: StudentDossier;
    @Output() deleteStudentDossier: EventEmitter<StudentDossier> = new EventEmitter<StudentDossier>();

    public maxPoints = MAX_POINTS;

    constructor() {
    }

    ngOnInit(): void {
    }

    deleteStudentDossierClick(): void {
        this.deleteStudentDossier.emit(this.studentDossier);
    }

}
