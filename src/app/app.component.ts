import {Component, OnInit} from '@angular/core';
import {StudentDossierService} from "./services/student-dossier.service";
import {StudentDossier} from "./models/StudentDossier";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    public studentDossiers: StudentDossier[];

    constructor(private studentDossierService: StudentDossierService) {
    }

    ngOnInit(): void {
        this.studentDossierService.getStudentDossiers().subscribe((data: StudentDossier[]) => {
            this.studentDossiers = data;
        });
    }

    studentDossierHeaderClick(): void {
        alert('Student Dossier Application');
    }

    deleteStudentDossier(studentDossier: StudentDossier, index: number): void {
        // console.log(studentDossier);
        this.studentDossiers.splice(index, 1);
    }
}
