import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {StudentDossier} from "../models/StudentDossier";

@Injectable({
  providedIn: 'root'
})
export class StudentDossierService {
  studentDossierUrl = 'assets/studentDossiers.json';

  constructor(private http: HttpClient) { }

  getStudentDossiers(): Observable<StudentDossier[]> {
    return this.http.get<StudentDossier[]>(this.studentDossierUrl);
  }
}
