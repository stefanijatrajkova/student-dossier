import { TestBed } from '@angular/core/testing';

import { StudentDossierService } from './student-dossier.service';

describe('StudentDossierService', () => {
  let service: StudentDossierService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StudentDossierService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
