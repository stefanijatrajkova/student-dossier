export interface StudentDossier {
  firstName: string;
  lastName:string;
  email: string;
  image: string;
  subject: string;
  points: number;
  birthday: Date;
}
